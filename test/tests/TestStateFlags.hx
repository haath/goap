package tests;

import types.TestState;
import utest.Assert;
import utest.ITest;


class TestStateFlags implements ITest
{
    public function new()
    {
    }

    function testStateFlags()
    {
        var s: TestState = Empty;
        Assert.isFalse(s.has(State1));
        Assert.isFalse(s.has(State2));
        Assert.isFalse(s.has(State3));
        Assert.isFalse(s.has(State4));
        Assert.isFalse(s.hasAll(State2 | State4));

        s.set(State2);
        Assert.isFalse(s.has(State1));
        Assert.isTrue(s.has(State2));
        Assert.isFalse(s.has(State3));
        Assert.isFalse(s.has(State4));
        Assert.isFalse(s.hasAll(State2 | State4));

        s.set(State4);
        Assert.isFalse(s.has(State1));
        Assert.isTrue(s.has(State2));
        Assert.isFalse(s.has(State3));
        Assert.isTrue(s.has(State4));
        Assert.isTrue(s.hasAll(State2 | State4));
    }
}
