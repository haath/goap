package types;

@:build(goap.macro.StateBuilder.build())
@:forward
enum abstract LumberjackState(Int) to Int
{
    var HasAxe;
    var HasWood;
    var NotTired;

    @:to
    public inline function toString(): String
    {
        var flags: Array<String> = [ ];

        if (cast(this, LumberjackState) & HasAxe)
        {
            flags.push('HasAxe');
        }
        if (cast(this, LumberjackState) & HasWood)
        {
            flags.push('HasWood');
        }
        if (cast(this, LumberjackState) & NotTired)
        {
            flags.push('NotTired');
        }

        return flags.join(',');
    }
}
