package types.actions;

import goap.Action;


class GetAxe extends Action<LumberjackState>
{
    public function new()
    {
        name = "GetAxe";
        preconditions = NotTired;
        effectSet = HasAxe;
        effectClear = NotTired;
    }
}
