#!/bin/bash

REF=${CI_COMMIT_TAG:-master}

haxelib run dox -o public -i build/types.xml \
    -in ^goap.*$ \
    -D source-path https://gitlab.com/haath/goap/-/tree/${REF}/src/ \
    -D website https://gitlab.com/haath/goap \
    -D logo https://gitlab.com/uploads/-/system/project/avatar/19075408/haxe-logo-glyph.png?width=64 \
    --title "GOAP API documentation" \
    -D description  "GOAP API documentation"

