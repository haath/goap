package goap.macro;

#if macro
import haxe.macro.Context;
import haxe.macro.Expr;

using StringTools;
using haxe.macro.ExprTools;
using haxe.macro.Tools;
using haxe.macro.TypeTools;


class StateBuilder
{
    public static macro function build(): Array<Field>
    {
        var fields: Array<Field> = Context.getBuildFields();
        var abstractPack: Array<String> = Context.getLocalClass().get().pack.slice(0, -1);
        var abstractName: String = Context.getLocalClass().get().name.replace('_Impl_', '');
        var abstractType: ComplexType = TPath({pack: abstractPack, name: abstractName});

        // add bitflag values to all state flags
        var bit: Int = 0;
        for (field in fields)
        {
            if (bit > 31)
            {
                Context.error('only a maximum of 32 state flags is supported', field.pos);
            }

            switch field.kind
            {
                case FVar(t, e):
                    {
                        if (e != null)
                        {
                            Context.error('state flag values should be left empty, they are automatically populated by the macro', field.pos);
                        }

                        field.kind = FVar(t, macro $v{1 << bit});
                        bit++;
                    }

                default:
            }
        }

        // add empty state
        fields.push(
            {
                name: 'Empty',
                pos: Context.currentPos(),
                kind: FVar(abstractType, macro 0),
                doc: 'The empty state with no flags set.'
            });

        // add bitwise or operator overload
        fields.push(
            {
                name: 'bitwiseOr',
                pos: Context.currentPos(),
                kind: FFun(
                    {
                        args: [
                            {
                                name: 'a',
                                type: abstractType
                            },
                            {
                                name: 'b',
                                type: abstractType
                            }
                        ],
                        ret: abstractType,
                        expr: Context.parse('return cast(cast(a, Int) | cast(b, Int), ${abstractName});', Context.currentPos())
                    }),
                meta: [
                    {name: ':op', pos: Context.currentPos(), params: [ macro a | b ]} ],
                access: [ AStatic, AInline ]
            });

        // add bitwise and operator overload
        fields.push(
            {
                name: 'bitwiseAnd',
                pos: Context.currentPos(),
                kind: FFun(
                    {
                        args: [
                            {
                                name: 'a',
                                type: abstractType
                            },
                            {
                                name: 'b',
                                type: abstractType
                            }
                        ],
                        ret: macro : Bool,
                        expr: Context.parse('return (cast(a, Int) & cast(b, Int)) > 0;', Context.currentPos())
                    }),
                meta: [
                    {name: ':op', pos: Context.currentPos(), params: [ macro a & b ]} ],
                access: [ AStatic, AInline ]
            });

        return fields;
    }
}
#end
