package goap;

enum abstract State(Int) from Int to Int
{
    public inline function new()
    {
        this = 0;
    }

    public inline function isEmpty(): Bool
    {
        return this == 0;
    }

    public inline function set(s: State)
    {
        this = this | s;
    }

    public inline function clear(s: State)
    {
        this = this & ~s;
    }

    public inline function setAndClear(set: State, clear: State)
    {
        this = (this | set) & ~clear;
    }

    public inline function has(s: State): Bool
    {
        return this & cast(s, Int) > 0;
    }

    public inline function hasAll(s: State): Bool
    {
        return (this & cast(s, Int)) == cast(s, Int);
    }
}
