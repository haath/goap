package goap;

import astar.types.Result;
import goap.Action;


typedef ActionPlan<S: Int> =
{
    var result: Result;
    var cost: Float;
    var plan: Array<Action<S>>;
}
